#include <iostream>

const int N = 11;

/* � �� ����� ��� �������� ��� if, ������ � ������ ���������� ���������� �����, a ? b : c
*  ������� ����� ������������ ��� ��� �� ������ ��� ������� ����������, � ����� ���� �����
*  ��� ����� ������ ����� �� 2 ������ ��� ���������� � �������� ���� ������ ������ �� 2 ��� ����������
*  � ������ ��������� 2 + 2 � ���������� ���:
*/

void FindOddEvenNumbers(int N, bool isEven) {
    for (int i = isEven ? 0 : 1; i <= N; i += 2) { 
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    std::cout << "All even numbers: ";
    FindOddEvenNumbers(N, true);

    std::cout << "All odd numbers: ";
    FindOddEvenNumbers(N, false);
}
